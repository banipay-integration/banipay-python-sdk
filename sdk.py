"""
Module: sdk
"""
from banipay.config import RequestOptions
from banipay.http import HttpClient
from banipay.resources import (
    CardToken,
    Payment,
    PaymentMethods,
    QRCode,
    User
)


class SDK:
    def __init__(
        self,
        affiliate,
        business,
        environment='Prod',
        http_client=None,
        request_options=None,
    ):
        """Construct your SDK Object to have access to all APIs modules.
        Args:
            affiliate: Affiliate Key
            business: Business Key
            environment: [Prod/Dev]
        Raises:
            ValueError: Param request_options must be a RequestOptions Object
        """

        self.http_client = http_client
        if http_client is None:
            self.http_client = HttpClient()

        self.request_options = request_options
        if request_options is None:
            self.request_options = RequestOptions()

        self.request_options.affiliate = affiliate
        self.request_options.business = business
        self.request_options.environment = environment

    def card_token(self, request_options=None):

        return CardToken(request_options is not None and request_options
                         or self.request_options, self.http_client)

    def payment(self, request_options=None):

        return Payment(request_options is not None and request_options
                       or self.request_options, self.http_client)

    def payment_methods(self, request_options=None):

        return PaymentMethods(request_options is not None and request_options
                              or self.request_options, self.http_client)

    def user(self, request_options=None):

        return User(request_options is not None and request_options
                              or self.request_options, self.http_client)

    def qr_code(self, request_options=None):

        return QRCode(request_options is not None and request_options
                           or self.request_options, self.http_client)

    @property
    def request_options(self):

        return self.__request_options

    @request_options.setter
    def request_options(self, value):
        if value is not None and not isinstance(value, RequestOptions):
            raise ValueError(
                "Param request_options must be a RequestOptions Object")
        self.__request_options = value

    @property
    def http_client(self):

        return self.__http_client

    @http_client.setter
    def http_client(self, value):
        if value is not None and not isinstance(value, HttpClient):
            raise ValueError("Param http_client must be a HttpClient Object")
        self.__http_client = value

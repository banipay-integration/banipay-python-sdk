# BaniPay SDK for Python


## 💡 Requirements

Python 3 or higher.


### Simple usage

```python
import banipay
import json

sdk = banipay.SDK(business="02e4b31f-20bd-43f9-9f2d-3ef7733f2d0f", affiliate="0ef291bf-0d70-444a-b949-b4144bba6557", environment="Dev")

bdd = [
{"title": "successUrl", "description": "https://google.com", "data": "https://google.com", "status": "true", "typeDate":"2022-09-22T08:05:24.688Z" },
{"title": "failUrl", "description": "https://google.com", "data": "https://google.com", "status": "true", "typeDate":"2022-09-22T08:05:24.688Z" },
{"title": "notificationUrl", "description": "https://google.com", "data": "https://google.com", "status": "true", "typeDate":"2022-09-22T08:05:24.688Z" }
]

payment_data = {"amount": 10.00,
"code": "ABC123",
"currency": "BOB",
"description": "Mi producto",
"expirationDate": "2022-09-22T08:05:24.688Z",
"productName": "Mi producto",
"bdd": json.dumps({"data": bdd})
};

result = sdk.payment().create(payment_data)
payment = result["response"]

print(payment)
```


### QR Code usage

```python
import banipay

sdk = banipay.SDK(business="02e4b31f-20bd-43f9-9f2d-3ef7733f2d0f", affiliate="0ef291bf-0d70-444a-b949-b4144bba6557", environment="Dev")

payment_data = {"amount": 10.00,
"code": "ABC123",
"currency": "BOB",
"description": "Mi producto",
"expiration": "29/00:00",
"gloss": "Mi producto",
"type": "banipay",
"idCommercial": "BP-1",
"singleUse": "true",
"paymentId": 0
};

result = sdk.qr_code().create(payment_data)
payment = result["response"]

print(payment)
```

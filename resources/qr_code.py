"""
    Module: qr_code
"""
from banipay.core import BPBase


class QRCode(BPBase):

    def create(self, payment_object, request_options=None):
        """

        Args:
            payment_object (dict): Payment to be created
            request_options (banipay.config.request_options, optional): An instance of
            RequestOptions can be pass changing or adding custom options to ur REST call.
            Defaults to None.

        Raises:
            ValueError: Param payment_object must be a Dictionary

        Returns:
            dict: Payment creation response
        """
        if not isinstance(payment_object, dict):
            raise ValueError("Param payment_object must be a Dictionary")
        resp = self._post(uri="/pagos/qr-payment", data=payment_object, request_options=request_options)
        return resp

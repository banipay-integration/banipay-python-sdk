"""
    Module: user
"""
from banipay.core import BPBase


class User(BPBase):
    """
    Access to Users, Will soon allow to create new Users
    """

    def get(self, request_options=None):
        return self._get(uri="/auth/api/business/" + str(self.request_options.business), request_options=request_options)

"""
    Module: payment
"""
from banipay.core import BPBase


class Payment(BPBase):
    """
    This class provides the methods to access the API that will allow you to create
    your own payment experience on your website.
    """

    def list(self, request_options=None):
        """

        Args:
            request_options (banipay.config.request_options, optional): An instance of
            RequestOptions can be pass changing or adding custom options to ur REST call.
            Defaults to None.

        Returns:
            dict: Payment find response
        """
        return self._get(uri="/pagos/payment/business/" + str(self.request_options.business),
                         request_options=request_options)


    def search(self, filters=None, request_options=None):
        """

        Args:
            request_options (banipay.config.request_options, optional): An instance of
            RequestOptions can be pass changing or adding custom options to ur REST call.
            Defaults to None.

        Returns:
            dict: Payment find response
        """
        urlSearch = "/pagos/payment/business/date/type-payment/" + str(filters.paymentMethod) \
                    + "/" + str(self.request_options.business) +"?dateEnd=" + str(filters.dateEnd) + "&dateStart=" + str(filters.dateStart)
        return self._get(uri=urlSearch,
                         request_options=request_options)

    def get(self, transaction_id, request_options=None):
        """

        Args:
            transaction_id (str): The Transaction ID
            request_options (banipay.config.request_options, optional): An instance of
            RequestOptions can be pass changing or adding custom options to ur REST call.
            Defaults to None.

        Returns:
            dict: Payment find response
        """
        return self._get(uri="/pagos/payment/transaction/" + str(transaction_id), request_options=request_options)

    def create(self, payment_object, request_options=None):
        """

        Args:
            payment_object (dict): Payment to be created
            request_options (banipay.config.request_options, optional): An instance of
            RequestOptions can be pass changing or adding custom options to ur REST call.
            Defaults to None.

        Raises:
            ValueError: Param payment_object must be a Dictionary

        Returns:
            dict: Payment creation response
        """
        if not isinstance(payment_object, dict):
            raise ValueError("Param payment_object must be a Dictionary")
        resp = self._post(uri="/pagos/link-payment", data=payment_object, request_options=request_options)
        base_url = "https://v2.banipay.me/modal/"
        environment = self.request_options.environment
        if environment == "Dev":
            base_url = "https://staging.banipay.me/modal/"
        resp['response']['url'] = base_url + str(resp['response']['id'])
        return resp

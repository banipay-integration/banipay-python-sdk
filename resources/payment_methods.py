"""
    Module: payment_methods
"""
from banipay.core import BPBase


class PaymentMethods(BPBase):
    """
    Access to Payment Methods
    """

    def list_all(self, request_options=None):
        return self._get(uri="/pagos/payment/business/method/" + str(self.request_options.business), request_options=request_options)

"""
Module: core/__init__.py
"""
from banipay.core.bp_base import BPBase


__all__ = (
    'BPBase',
)

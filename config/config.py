"""
Module: config
"""
import platform


class Config:
    """
    General infos of your SDK
    """

    def __init__(self):
        self.__version = "0.1.0"
        self.__user_agent = "BaniPay Python SDK v" + self.__version

    __api_base_url = "https://v2.banipay.me/api"
    __api_test_base_url = "https://staging.banipay.me/api"
    __mime_json = "application/json"
    __mime_form = "application/x-www-form-urlencoded"

    @property
    def version(self):
        """
        Sets the attribute value of version
        """
        return self.__version

    @property
    def user_agent(self):
        """
        Sets the attribute value of user agent
        """
        return self.__user_agent

    @property
    def api_base_url(self):
        """
        Sets the attribute value of api base url
        """
        return self.__api_base_url

    @property
    def api_test_base_url(self):
        """
        Sets the attribute value of test api base url
        """
        return self.__api_test_base_url

    @property
    def mime_json(self):
        """
        Sets the attribute value of mime json
        """
        return self.__mime_json

    @property
    def mime_form(self):
        """
        Sets the attribute value of mime form
        """
        return self.__mime_form
